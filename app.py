#!/usr/bin/python
#-*-coding:utf-8-*-

import tornado.ioloop
import tornado.template
import tornado.httpserver
from settings import *
import tornado.wsgi
from tornado.web import asynchronous,authenticated
from auth.user import Login,Register,UpdateToken,UpdateAvatar,\
	GetAvatar,UpdateUserName,CheckLogin
from record.record import Record
from relationship.relationship import AddFriend,GetFriends,\
	GetFriendsAndMiss,UpdateFriend
from miss.miss import SendMiss,GetMissNum,\
	GetMiss
from audio.audio import AudioRemind,GetRemindNum,\
	GetRemind,GetRemindAudio,GetRemindLimitFifty,GetRemindLimitXrange
from rank.rank import Rank
from session import BaseHandler
import sys
import time

class MainHandler(BaseHandler):
	@asynchronous
	@authenticated
	def get(self):
		usetime = self.request.request_time()
		self.render("index.html",usetime=usetime)
	def post(self):
		pass
	def safe_finish(self):
		pass


#This is the main view of hosts
def main(port):
	application = tornado.web.Application([
		(r"/",MainHandler),
		(r"/check_login/",CheckLogin),
		(r"/send_remind_to_relative/",AudioRemind),
		(r"/add_relatives/",AddFriend),
		(r"/update_contactName/",UpdateFriend),
		(r"/get_relatives_list_with_miss/",GetFriendsAndMiss),
		(r"/get_relatives_list/",GetFriends),
		(r"/login/",Login),
		(r"/register/",Register),
		(r"/update_avatar/",UpdateAvatar),
		(r"/get_avatar/",GetAvatar),
		(r"/update_token/",UpdateToken),
		(r"/upload_user_data/",Record),
		(r"/update_user_name/",UpdateUserName),
		(r"/get_remind_audio/",GetRemindAudio),
		(r"/get_remind_num/",GetRemindNum),
		(r"/get_remind/",GetRemind),
		(r"/get_remind_fifty_limit/",GetRemindLimitFifty),
		(r"/get_remind_xrange_limit/",GetRemindLimitXrange),
		(r"/record/",Record),
		(r"/send_miss_to_relative/",SendMiss),
		(r"/get_miss_num/",GetMissNum),
		(r"/get_miss/",GetMiss),
		(r"/get_intimacy/",Rank)
		],**settings)
	server = tornado.httpserver.HTTPServer(application)
	server.listen(port)
	tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
	main(int(sys.argv[1]))