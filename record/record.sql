create database if not exists buddy;
use buddy;

create table record(
	`userid` int not null,
	`friendid` int not null,
	`type_id` int not null,
	`time` int not null
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

