#!/usr/bin/python
#-*-coding:utf-8-*-

from settings import *
from tornado.options import define,options
import tornado.template
from tornado.web import asynchronous
from tornado.web import authenticated
from session import BaseHandler
from databases import DB,Redis
import json


import sys,math
import time

def getvalue(curvalue,contactTime):
	dist = int(time.time()) - contactTime
	rate = 0.1*math.tanh(curvalue)
	value = math.pow(0.95-rate, 0.00002*dist)*curvalue
	return value

def AddScore(uid,friendid,typeId):
	x = SCORE[typeId]/10.0
	db = DB()
	sql = "select intimateValue,contactTime from relation where userid=%d \
			and friendid=%d" % (uid,friendid)
	intimatevalues = db.Query(sql)
	intimatevalue = float(intimatevalues[0][0])
	contactTime = int(intimatevalues[0][1])
	userValue = getvalue(intimatevalue,contactTime)
	contactTime = int(time.time())
	userValue += (1.0 - math.tanh(userValue))*x
	userValue = min(userValue,1.0)
	sql = "update relation set intimateValue=%f,contactTime=%d where userid=%d \
			and friendid=%d" % (userValue,contactTime,uid,friendid)
	db.Execute(sql)
	sql2 = "update relation set intimateValue=%f,contactTime=%d where userid=%d \
			and friendid=%d" % (userValue,contactTime,friendid,uid)
	db.Execute(sql2)


#user data record.
class Record(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		friendPhoneNumber = int(self.get_argument('friendPhoneNumber'))
		typeId = int(self.get_argument('typeId'))
		uid = self.get_userid
		nowtime = int(time.time())
		db = DB()
		friendid = db.GetId(friendPhoneNumber)
		sql = "insert into record(userid,friendid,type_id,\
			time) value(%d,%d,%d,%d)" %(uid,friendid,typeId,nowtime)
		AddScore(uid, friendid, typeId)
		db.Execute(sql)
		data = {'status_code':STATUS_OK}
		self.api_response(data)
	def get(self):
		pass





