#!/usr/bin/python
#-*-coding:utf-8-*-

# 亲密度算法
#
#

from settings import *
from tornado.options import define,options
import tornado.template
from tornado.web import asynchronous
from tornado.web import authenticated
from session import BaseHandler
from databases import DB,Redis
import json,random

import sys,math,time

def getvalue(curvalue,contactTime):
	dist = int(time.time()) - contactTime
	rate = 0.1*math.tanh(curvalue)
	value = math.pow(0.95-rate, 0.00002*dist)*curvalue
	return value

class Rank(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		db = DB()
		friendPhoneNumber = int(self.get_argument('friendPhoneNumber'))
		friendid = db.GetId(friendPhoneNumber)
		uid = self.get_userid
		sql = "select intimateValue,contactTime from relation where userid=%d \
				and friendid=%d" % (uid,friendid)
		intimatevalues = db.Query(sql)
		intimatevalue = float(intimatevalues[0][0])
		contactTime = int(intimatevalues[0][1])
		userValue = int(getvalue(intimatevalue,contactTime)*100)
		data = {"status_code":STATUS_OK,"userIntimacy":userValue}
		self.api_response(data)
	@authenticated
	def get(self):
		pass



