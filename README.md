# Buddy 接口 #

## 注册模块
### URL: /register/
#### Method:POST
#### Parameters:
    {
        userType:int,
        phoneNumber:long,
        userName:string,
        password:string,
        email:string
    }

## 判断是否登陆
### URL: /check_login/
#### Method:POST
#### Return:
    {
        'status_code':int
    }
    
## 更新pushToken
### URL: /update_token/
#### Method:POST
    {
        pushToken:string
    }

## 更新用户名
### URL: /update_user_name/
#### Method:POST
    {
        userName:string
    }


## 更新头像信息
### URL: /update_avatar/
#### Method:POST
    {
        avatar:file
    }
`登陆后才能获取`
## 获取头像信息
### URL:/get_avatar/
#### Method:POST
    {}
### 返回信息
    {"status_code":1,"avatar_url":"string"}
`根据返回的url，进行GET，就可以获取到头像文件了`
## 登陆
### URL： /login/
#### Method:POST
    {
        phoneNumber:long,
        password:string
    }
```登陆后，cookie中将记录一个唯一的token，表示某个用户```
 {"status_code":1,"user_type":0 or 1}
## 添加亲人好友
### URL: /add_relatives/
#### Method:POST
    {
        contactName:string,   ```非必须```
        phoneNumber:long
    }

## 修改备注
### URL: /update_contactName/
#### Method:POST
    {
        contactName:string,   
        friendPhoneNumber:long
    }

## 拉去快速通话界面
### URL: /get_relatives_list/
    {
        'status_code':int,
        'data':[
            {
                contactName:string,
                avatarUrl:string,
                phoneNumber:long
            }
            ,
            {....}
        ]
    }

## 拉去快速通话界面2
### URL: /get_relatives_list_with_miss/
    {
        'status_code':int,
        'data':[
            {
                contactName:string,
                avatarUrl:string,
                phoneNumber:long,
                miss:(true/false),
                userIntimacy:int
            }
            ,
            {....}
        ]
    }

## 发送语音提醒给亲人
### URL: /send_remind_to_relative/
#### Method:POST
     {
        audio:file, #语言文件
        friendPhoneNumber:long,  #亲人号码
        remindTime:int   #提醒的时间
     }

## 获取语音提醒数目
### URL: /get_remind_num/
#### Method:POST
    {"status_code": int, "remind_num": int}

## 获取语音提醒信息
### URL: /get_remind/
#### Method:POST
    {
    "status_code": int,
    'data':[
        {"audio": url, "phoneNumber": int, "remindTime": int}
        ,
        {}...
        ]
    }

## 获取语音提醒信息
### URL: /get_remind_fifty_limit/
#### Method:POST
    {
    "status_code": int,
    'data':[
        {"audio": url, "phoneNumber": int, "remindTime": int}
        ,
        {}...
        ]
    }

## 获取语音提醒信息
### URL: /get_remind_xrange_limit/
#### Method:POST
    {
        'lrange':int,
        'rrange':int    ```lrange < rrange```
    }
#### Return
    {
    "status_code": int,
    'data':[
        {"audio": url, "phoneNumber": int, "remindTime": int}
        ,
        {}...
        ]
    }

## 获取语音文件
### URL: /get_remind_audio/
#### Method:POST
    from /get_remind/  we get the audio url,post or get , can download audio file
    audio: file

## 发送思念给亲人
### URL: /send_miss_to_relative/
#### Method:POST
     {
        friendPhoneNumber:long,  #亲人号码
     }

## 获取思念数目
### URL: /get_miss_num/
#### Method:POST
     {"status_code": int, "miss_num": int}

## 获取思念
### URL: /get_miss/
#### Method:POST
     {
            "status_code":1
        ,
        "data":[
        {
            "misstime":int,
            "phoneNumber":int
        },
        {
            "misstime":int,
            "phoneNumber":int
        }
        ...
        }
    }

## 
### URL: /get_intimacy/
#### Method: POST
    {
        "friendPhoneNumber":long,
    }
##### RETURN
    {'status_code':int,'userIntimacy':int}
    ```And userIntimacy in  (1~100)```

## 记录操作
### URL: /record/
#### Method:POST
    {
        "friendPhoneNumber":long,
        "typeId":int
    }

### Return: json
#### ok:
    {
        status_code:1
    }
#### error:
    {
        目前status_code有：
        STATUS_OK = 1
        STATUS_PARAMS = 2 #参数缺失
        STATUS_PARAMS_ERROR = 3 #参数类型错误
        STATUS_PASS_ERROR = 4  #登陆密码错误
        STATUS_NAME_ERROR = 5  #登陆用户名错误
        STATUS_FILE_ERROR = 6 #文件过大 or 没有
        STATUS_FILE_MISS = 7 #文件不见了
        STATUS_FRIEND_MISS = 8 #好友没注册哦
        STATUS_FRIEND_ACCEPT = 9 #添加好友成功
        STATUS_FRIEND_WA = 10 #添加好友失败
        STATUS_NOT_LOGIN = 11 #没有登陆
    }


#### record:  
    ```记录用户操作```
    {
        目前typeId有：
        TYPE_TELEPHONE = 1  #打电话  6
        TYPE_VOICE_REMIND = 2 #语言提醒 3
        TYPE_SMS = 3 #短信 3
        TYPE_MISS = 4 #发送思念 3
    }
    ...  /*以后添加*/
    
``` user = phoneNumber-id```


```按照标准是get获取信息。post用于更新数据的操作。
这里还是多使用post来获取信息。```

### miss  audio 的发送与否将存在redis中
### 暂时后面编写的都不加容错了。