#!/usr/bin/python
#-*-coding:utf-8-*-

from settings import *
from tornado.options import define,options
import tornado.template
from tornado.web import asynchronous
from tornado.web import authenticated
from session import BaseHandler
from databases import DB,Redis
import json

import sys,time,hashlib,os

class SendMiss(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		uid = self.get_userid
		friendPhoneNumber = int(self.get_argument('friendPhoneNumber'))
		db = DB()
		friendid = db.GetId(friendPhoneNumber)
		nowtime = int(time.time())
		sql = "insert into miss(userid,friendid,missTime) value\
		(%d,%d,%d)" %(uid,friendid,nowtime)
		db.Execute(sql)
		data = {'status_code':STATUS_OK}
		myredis = Redis()
		myredis.HIncrby("Miss",friendid,1)
		self.api_response(data)
	def get(self):
		pass

class GetMissNum(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		uid = self.get_userid
		myredis = Redis()
		missNum = myredis.HGet("Miss",uid)
		if not missNum:
			missNum = 0
		else:
			missNum = int(missNum)
		data =  {'status_code':STATUS_OK}
		data.append({'miss_num':missNum})
		self.api_response(data)

	def get(self):
		uid = self.get_userid
		myredis = Redis()
		missNum = myredis.HGet("Miss",uid)
		if not missNum:
			missNum = 0
		else:
			missNum = int(missNum)
		data =  {'status_code':STATUS_OK,'miss_num':missNum}
		self.api_response(data)

class GetMiss(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		uid = self.get_userid
		myredis = Redis()
		missNum = myredis.HGet("Miss",uid)
		if not missNum:
			missNum = 0
		else:
			missNum = int(missNum)
		db = DB()
		sql = "select userid,missTime from miss where\
		friendid=%d order by missTime desc limit %d" %(uid,missNum)
		resultval = db.Query(sql)
		misslist = []
		for miss in resultval:
			friendid = int(miss[0])
			sql = "select phoneNumber from user where id=%d" % friendid 
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			usermiss = {'phoneNumber':phoneNumber,'misstime':miss[1]}
			misslist.append(usermiss)
		myredis.HSet("Miss",uid,0)
		data = {"status_code":STATUS_OK,"data":misslist}
		self.api_response(data)
	def get(self):
		pass

