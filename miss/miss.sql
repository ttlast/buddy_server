create database if not exists buddy;
use buddy;

create table miss(
	`userid`	int not null,
	`friendid` int not null,
	`missTime` int not null
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
