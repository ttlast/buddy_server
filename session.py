#!/usr/bin/python
#-*-coding:utf-8-*-

import cPickle as pickle
from uuid import uuid4
import time
import tornado
import redis
from pycket.session import SessionManager
from json import dumps

class BaseHandler(tornado.web.RequestHandler):
	def get_current_user(self):
		return self.session.get('user')

	def api_response(self,data):
		self.set_header('Content-Type','application/json; charset=UTF-8')
		data = dumps(data,ensure_ascii=False)
		self.finish(data)
	@property
	def session(self):
		return SessionManager(self)
	@property
	def get_userid(self):
		return int(self.session.get('user').split('-')[1])
