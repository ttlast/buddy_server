#!/usr/bin/python
#-*-coding:utf-8-*-

from settings import *
from tornado.options import define,options
import tornado.template
from tornado.web import asynchronous
from tornado.web import authenticated
from session import BaseHandler
from databases import DB,Redis
import json

import sys,time,math

reload(sys)
sys.setdefaultencoding('utf-8')

class UpdateFriend(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		uid = self.get_userid
		db = DB()
		friendPhoneNumber = int(self.get_argument('friendPhoneNumber'))
		friendid = db.GetId(friendPhoneNumber)
		username = self.get_argument('contactName','')  ##可以被注入，后续dei unescape
		sql = "update relation set contactName='%s' where userid=%d \
				and friendid=%d" % (username,uid,friendid)
		db.Execute(sql)
		data = {'status_code':STATUS_OK}
		self.api_response(data);
	def get(self):
		pass

class AddFriend(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		try:
			username = self.get_argument('contactName','')
			phoneNumber = int(self.get_argument('phoneNumber'))
			print "%s %d" % (username,phoneNumber)
		except:
			data =  {'status_code':STATUS_PARAMS_ERROR}
			self.api_response(data)
		else:
			sql = "select userName,id from user where phoneNumber=%d" \
				% (phoneNumber)
			db = DB()
			try:
				value = db.Query(sql)
			except:
				data =  {'status_code':STATUS_PARAMS_ERROR}
				self.api_response(data)
			else:
				if value:
					if username == '':
						username = value[0][0]
					friendid = value[0][1]
					svalue = self.session.get('user')
					uid = int(svalue.split('-')[1])
					contactName = username
					sql = "insert into relation(userid,friendid,contactName)\
					value(%d,%d,'%s')" %(uid,friendid,contactName)
					sql2 = "insert into relation(userid,friendid)\
					value(%d,%d)" %(friendid,uid)
					try:
						db.Execute(sql)
						if uid != friendid:
							db.Execute(sql2)
						data = {'status_code':STATUS_FRIEND_ACCEPT}
					except:
						data = {'status_code':STATUS_FRIEND_WA}
					self.api_response(data)
				else:
					data =  {'status_code':STATUS_FRIEND_MISS}
					self.api_response(data);
	@authenticated
	def get(self):
		self.write('<html><body>\
				<form action="" method="post">\
				<p> contactName: <input type="text" name="contactName"> </p>\
				<p> phoneNumber: <input type="text" name="phoneNumber"> </p>\
				<input type="submit" value="Sign in"></form></body></html>')
		self.finish()

class GetFriends(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		uid = self.get_userid
		sql = "select friendid,contactName from relation where userid=%d" % uid
		db = DB()
		values = db.Query(sql)
		friendslist = []
		for value in values:
			friendid = int(value[0])
			contactName = value[1]
			sql = "select phoneNumber from user where id=%d" % friendid
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			avatarFile = "%s/%d" %(options.avatarpath,friendid)
			avatarUrl = options.host + "/get_avatar/?file=%s" % avatarFile
			friends={'contactName':contactName,'avatarUrl':avatarUrl,\
			'phoneNumber':phoneNumber}
			friendslist.append(friends)
		data = {'status_code':STATUS_OK,"data":friendslist}
		self.api_response(data)

	@asynchronous
	@authenticated
	def get(self):
		uid = self.get_userid
		sql = "select friendid,contactName from relation where userid=%d" % uid
		db = DB()
		values = db.Query(sql)
		friendslist = []
		for value in values:
			friendid = int(value[0])
			contactName = value[1]
			sql = "select phoneNumber from user where id=%d" % friendid
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			avatarFile = "%s/%d" %(options.avatarpath,friendid)
			avatarUrl = options.host + "/get_avatar/?file=%s" % avatarFile
			friends={'contactName':contactName,'avatarUrl':avatarUrl,\
			'phoneNumber':phoneNumber}
			friendslist.append(friends)
		data = {'status_code':STATUS_OK,"data":friendslist}
		self.api_response(data)

def getvalue(curvalue,contactTime):
	dist = int(time.time()) - contactTime
	rate = 0.1*math.tanh(curvalue)
	value = math.pow(0.95-rate, 0.00002*dist)*curvalue
	return int(value*100)

class GetFriendsAndMiss(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		uid = self.get_userid
		sql = "select friendid,contactName,intimateValue,contactTime from relation where userid=%d" % uid
		db = DB()
		values = db.Query(sql)
		friendslist = []
		for value in values:
			friendid = int(value[0])
			contactName = value[1]
			intimateValue = float(value[2])
			contactTime = int(value[3])
			userValue = getvalue(intimateValue,contactTime)
			sql = "select phoneNumber from user where id=%d" % friendid
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			avatarFile = "%s/%d" %(options.avatarpath,friendid)
			avatarUrl = options.host + "/get_avatar/?file=%s" % avatarFile
			isMiss = False
			newtime = int (time.time()-24*60*60)
			sql = "select * from miss where userid=%d and friendid=%d \
			and missTime>%d" % (friendid,uid,newtime)
			isMiss = db.Query(sql)
			if isMiss:
				isMiss = True
			else:
				isMiss = False
			friends={'contactName':contactName,'avatarUrl':avatarUrl,\
			'phoneNumber':phoneNumber,'miss':isMiss,'userIntimacy':userValue}
			friendslist.append(friends)
		data = {'status_code':STATUS_OK,"data":friendslist}
		self.api_response(data)

	@asynchronous
	@authenticated
	def get(self):
		uid = self.get_userid
		sql = "select friendid,contactName,intimateValue,contactTime from relation where userid=%d" % uid
		db = DB()
		values = db.Query(sql)
		friendslist = []
		for value in values:
			friendid = int(value[0])
			contactName = value[1]
			intimateValue = float(value[2])
			contactTime = int(value[3])
			userValue = int(getvalue(intimateValue,contactTime)*100)
			sql = "select phoneNumber from user where id=%d" % friendid
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			avatarFile = "%s/%d" %(options.avatarpath,friendid)
			avatarUrl = options.host + "/get_avatar/?file=%s" % avatarFile
			isMiss = False
			newtime = int (time.time()-24*60*60)
			sql = "select * from miss where userid=%d and friendid=%d \
			and missTime>%d" % (friendid,uid,newtime)
			isMiss = db.Query(sql)
			if isMiss:
				isMiss = True
			else:
				isMiss = False
			friends={'contactName':contactName,'avatarUrl':avatarUrl,\
			'phoneNumber':phoneNumber,'miss':isMiss,'userIntimacy':userValue}
			friendslist.append(friends)
		data = {'status_code':STATUS_OK,"data":friendslist}
		self.api_response(data)

