create database if not exists buddy;
use buddy;

create table relation(
	`userid` int not null,
	`friendid` int  not null,
	`contactName`  varchar(20) DEFAULT null,
	primary key(`userid`,`friendid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table relation add intimateValue float(9,3) DEFAULT 0.60;
alter table relation add contactTime int DEFAULT 1380362174;