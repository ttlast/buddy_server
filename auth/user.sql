create database if not exists buddy;
use buddy;

create table user(
	`id` int not null auto_increment,
	`userType` int not null,
	`phoneNumber` bigint  not null unique,
	`password` varchar(50) not null,
	`email` varchar(60) DEFAULT null,
	`userName` varchar(20) DEFAULT null,
	`pushToken` varchar(60) DEFAULT null,
	primary key(`id`,`phoneNumber`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

