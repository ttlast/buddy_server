#!/usr/bin/python
#-*-coding:utf-8-*-

from settings import *
from tornado.options import define,options
import tornado.template
from tornado.web import asynchronous
from tornado.web import authenticated
from session import BaseHandler
from databases import DB,Redis
import json


import sys
import time

#注册接口
class Register(BaseHandler):
	@asynchronous
	def post(self):
		try:
			userName = self.get_argument('userName')
			userType = int(self.get_argument('userType'))
			phoneNumber = int(self.get_argument('phoneNumber'))
			password = self.get_argument('password')
			email = self.get_argument('email')
		except:
			data = {'status_code':STATUS_PARAMS}
			self.api_response(data)
		else:
			sql = "insert into user(userType,phoneNumber,userName,\
				password,email) value(%d,%d,'%s','%s','%s')" \
				% (userType,phoneNumber,userName,password,email)
			db = DB()
			if db.Execute(sql):
				data = {'status_code':STATUS_OK}
			else:
				data = {'status_code':STATUS_PARAMS_ERROR}
			self.api_response(data)

	@asynchronous  #for debug
	def get(self):
		self.write('<html><body><form action="" method="post">\
				<p>userType: <input type="text" name="userType"></p>\
				<p> phoneNumber: <input type="text" name="phoneNumber"> </p>\
				<p> password: <input type="password" name="password"> </p>\
				<p> email: <input type="text" name="email"> </p>\
				<input type="submit" value="Sign in"></form></body></html>')
		self.finish()

#登陆接口
class Login(BaseHandler):
	@asynchronous
	def post(self):
		try:
			phoneNumber = int(self.get_argument('phoneNumber'))
			password = self.get_argument('password')
		except:
			data = {'status_code':STATUS_PARAMS}
		else:
			sql = "select password,id,userType from user where phoneNumber=%d" \
				% (phoneNumber)
			db = DB()
			try:
				value = db.Query(sql)
			except:
				data = {'status_code':STATUS_PARAMS_ERROR}
			else:
				if value:
					if value[0][0] == password:
						svalue = "%d-%d" % (phoneNumber,value[0][1])
						self.session.set('user',svalue)
						data =  {'status_code':STATUS_OK,'user_type':value[0][2]}
					else:
						data = {'status_code':STATUS_PASS_ERROR}
				else:
					data = {'status_code':STATUS_NAME_ERROR}
		self.api_response(data)
	@asynchronous #for debug
	def get(self):
		self.write('<html><body>\
				<form action="" method="post">\
				<p> phoneNumber: <input type="text" name="phoneNumber"> </p>\
				<p> password: <input type="password" name="password"> </p>\
				<input type="submit" value="Sign in"></form></body></html>')
		self.finish()

#更新APNS token接口
class UpdateToken(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		pushToken = self.get_argument('pushToken')
		svalue = self.session.get('user')
		uid = int(svalue.split('-')[1])
		sql = "update user set pushToken='%s' where id=%d" % (pushToken,uid)
		db = DB()
		try:
			db.Execute(sql)
			data =  {'status_code':STATUS_OK}
		except:
			data = {'status_code':STATUS_PARAMS_ERROR}
		self.api_response(data)

#更新USER Name接口
class UpdateUserName(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		userName = self.get_argument('userName')
		svalue = self.session.get('user')
		uid = int(svalue.split('-')[1])
		sql = "update user set userName='%s' where id=%d" % (userName,uid)
		db = DB()
		try:
			db.Execute(sql)
			data =  {'status_code':STATUS_OK}
		except:
			data = {'status_code':STATUS_PARAMS_ERROR}
		self.api_response(data)


#更新头像接口
class UpdateAvatar(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		try:
			svalue = self.session.get('user')
			filename = int(svalue.split('-')[1])
			avatarinfo = self.request.files['avatar'][0]  #a list of files
			if len(avatarinfo['body']) > 4*1024*1024:
				data = {'status_code':STATUS_FILE_ERROR}
			else:
				avatar_file = "%s/%d" %(options.avatarpath,filename)
				file_obj = open(avatar_file,"wb")
				file_obj.write(avatarinfo['body'])
				file_obj.close()
				data =  {'status_code':STATUS_OK}
		except:
			data = {'status_code':STATUS_PARAMS}
		self.api_response(data)
	#for debug
	@authenticated
	def get(self):
		self.write('<html><body>\
				<form action="" enctype="multipart/form-data" method="post">\
				<p> avatar_file: <input type="file" name="avatar"> </p>\
				<input type="submit" value="Sign in"></form></body></html>')
		self.finish()

#获取头像接口
class GetAvatar(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		try:
			svalue = self.session.get('user')
			filename = int(svalue.split('-')[1])
			try:
				avatar_file = "%s/%d" %(options.avatarpath,filename)
				avatar_url = options.host + "/get_avatar/?file=%s" % avatar_file
				data =  {'status_code':STATUS_OK,'avatar_url':avatar_url}
			except:
				data = {'status_code':STATUS_FILE_MISS}
		except:
			data = {'status_code':STATUS_PARAMS}
		self.api_response(data)

	#有漏洞，可以看别人的头像，故意搞的
	#而且直接这样读取很大安全隐患
	@asynchronous
	#@authenticated
	def get(self):
		self.set_header('Content-Type','image/jpeg')
		avatar_path = self.get_argument('file')
		file_obj = open(avatar_path,"rb")
		while True:
			val = file_obj.read(options.chunksize)
			if val:
				self.write(val)
			else:
				break
		file_obj.close()
		self.finish()

#判断是否登陆 
class CheckLogin(BaseHandler):
	def post(self):
		if self.session.get('user'):
			db = DB()
			userid = self.get_userid
			userType = db.GetUserType(userid)
			data = {'status_code':STATUS_OK,'usertype':userType}
		else:
			data = {'status_code':STATUS_NOT_LOGIN}
		self.api_response(data)
	def get(self):
		if self.session.get('user'):
			db = DB()
			userid = self.get_userid
			userType = db.GetUserType(userid)
			data = {'status_code':STATUS_OK,'usertype':userType}
		else:
			data = {'status_code':STATUS_NOT_LOGIN}
		self.api_response(data)

