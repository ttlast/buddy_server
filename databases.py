#!/usr/bin/python
#-*-coding:utf-8-*-

from settings import *
from tornado.options import define,options
import redis
import time
import MySQLdb

def singleton(cls,*args,**kwargs):
	instances = {}
	def _singleton():
		if cls not in instances:
			instances[cls] = cls(*args,**kwargs)
		return instances[cls]
	return _singleton

@singleton
class DB(object):
	def __init__(self,db=options.mysql_db,host=options.mysql_host,\
			user=options.mysql_user,passwd=options.mysql_passwd):
		self.db = db
		self.host = host
		self.user = user
		self.passwd = passwd

	def _check_con(self):
		try:
			self.conn.ping()
			return True
		except:
			return False

	def _conn(self):
		try:
			self.conn = MySQLdb.connect(user=self.user,db=self.db,\
				passwd=self.passwd,host=self.host,charset='utf8')
			self.cur = self.conn.cursor()
			return True
		except:
			return False

	def _checkConn(func):
		stime = 3
		def _reConn(self,*args,**kwargs):
			while True:
				if self._check_con():
					return func(self,*args,**kwargs)
				else:
					if self._conn():
						return func(self,*args,**kwargs)
					else:
						time.sleep(stime)
						print 'sleep'
		return _reConn

	@_checkConn
	def Query(self,sql=''):
		try:
			self.cur.execute(sql)
			values = self.cur.fetchall()
			self.conn.commit()
			return values
		except:
			return []

	@_checkConn
	def Execute(self,sql=''):
		try:
			self.cur.execute(sql)
			self.conn.commit()
			return True
		except  :
			return False

	def GetId(self,phoneNumber):
		sql = "select id from user where phoneNumber=%d" \
			% (phoneNumber)
		values = self.Query(sql)
		if values:
			return int(values[0][0])
		else:
			return None

	def GetUserType(self,id):
		sql = "select userType from user where id=%d" % (id)
		values = self.Query(sql)
		if values:
			return int(values[0][0])
		else:
			return None

@singleton
class Redis():
	def __init__(self):
		self.pool = redis.ConnectionPool(host=options.redishost,\
			port=options.redisport)

	def Set(self,key,value):
		r = redis.Redis(connection_pool=self.pool)
		return r.set(key)

	def Get(self,key,value):
		r = redis.Redis(connection_pool=self.pool)
		return r.get(key)

	def HSet(self,key,field,value):
		r = redis.Redis(connection_pool=self.pool)
		return r.hset(key,field,value)

	def HGet(self,key,field):
		r = redis.Redis(connection_pool=self.pool)
		value = r.hget(key,field)
		return value

	def HGetALL(self,key):
		r = redis.Redis(connection_pool=self.pool)
		return r.hgetall(key)

	def HIncrby(self,key,field,increment):
		r = redis.Redis(connection_pool=self.pool)
		return r.hincrby(key,field,increment)

	def HDel(self,key,field):
		r = redis.Redis(connection_pool=self.pool)
		return r.hdel(key,field)

	def HLen(self,key):
		r = redis.Redis(connection_pool=self.pool)
		return r.hlen(key)

