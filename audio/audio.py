#!/usr/bin/python
#-*-coding:utf-8-*-

from settings import *
from tornado.options import define,options
import tornado.template
from tornado.web import asynchronous
from tornado.web import authenticated
from session import BaseHandler
from databases import DB,Redis
import json

import sys,time,hashlib,os

def GetRemindFile(filetime,uid):
	nowtime = str(filetime)+str(uid)
	filedate = time.strftime('%Y-%m-%d',time.localtime(filetime))
	filename = hashlib.md5(nowtime).hexdigest()
	return "%s/%s/%s" % (options.audiopath,filedate,filename)

class AudioRemind(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
	#	try:
		remindTime = int(self.get_argument('remindTime'))
		friendPhoneNumber = int(self.get_argument('friendPhoneNumber'))
		db = DB()
		friendid = db.GetId(friendPhoneNumber)
		svalue = self.session.get('user')
		uid = int(svalue.split('-')[1])
		nowtime = int(time.time())
		filedate = time.strftime('%Y-%m-%d',time.localtime(nowtime))
		filename=hashlib.md5(str(nowtime)+str(friendid)).hexdigest()
		remindinfo = self.request.files['audio'][0]  #a list of files
		if len(remindinfo['body']) > 4*1024*1024:
			data = {'status_code':STATUS_FILE_ERROR}
		else:
			remind_file = "%s/%s/%s" %(options.audiopath,filedate,filename)
			try:
				file_obj = open(remind_file,"wb")
			except:
				os.mkdir("%s/%s" % (options.audiopath,filedate))
				file_obj = open(remind_file,"wb")
			file_obj.write(remindinfo['body'])
			file_obj.close()
			data =  {'status_code':STATUS_OK}
			sql = "insert into audio(userid,friendid,time,remindTime) value\
			(%d,%d,%d,%d)" %(uid,friendid,nowtime,remindTime)
			db.Execute(sql)
			myredis = Redis()		#添加接收用户有多少条提醒
			myredis.HIncrby("Remind",friendid,1)
	#	except:
	#		data = {'status_code':STATUS_PARAMS}
		self.api_response(data)
	#for debug
	@authenticated
	def get(self):
		self.write('<html><body>\
				<form action="" enctype="multipart/form-data" method="post">\
				<p> audio: <input type="file" name="audio"> </p>\
				<p> friendPhoneNumber: <input type="text" name="friendPhoneNumber"> </p>\
				<p> remindTime: <input type="text" name="remindTime"> </p>\
				<input type="submit" value="Sign in"></form></body></html>')
		self.finish()

#收到的留言数,获取后，不清空
class GetRemindNum(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		uid = self.get_userid
		myredis = Redis()		#添加接收用户有多少条提醒
		remindNum = myredis.HGet("Remind",uid)
		if not remindNum:
			remindNum = 0
		else:
			remindNum = int(remindNum)
		data =  {'status_code':STATUS_OK,'remind_num':remindNum}
		self.api_response(data)
	def get(self):
		uid = self.get_userid
		myredis = Redis()		#添加接收用户有多少条提醒
		remindNum = myredis.HGet("Remind",uid)
		if not remindNum:
			remindNum = 0
		else:
			remindNum = int(remindNum)
		data =  {'status_code':STATUS_OK,'remind_num':remindNum}
		self.api_response(data)

class GetRemindAudio(BaseHandler):
	@asynchronous
	@authenticated
	def post(self):
		self.set_header('Content-Type','audio/aac')
		uid = self.get_userid
		nowtime = int(self.get_argument("time"))
		filepath = GetRemindFile(nowtime,uid)
		fd = open(filepath,"rb")
		while True:
			send = fd.read(options.chunksize)
			if not send:
				break
			self.write(send)
		fd.close()
		self.finish()
	@asynchronous
	@authenticated
	def get(self):
		self.set_header('Content-Type','audio/aac')
		uid = self.get_userid
		nowtime = int(self.get_argument("time"))
		filepath = GetRemindFile(nowtime,uid)
		print filepath
		fd = open(filepath,"rb")
		while True:
			send = fd.read(options.chunksize)
			if not send:
				break
			self.write(send)
		fd.close()
		self.finish()



#返回最近的收到的留言
#获取后，清空
class GetRemind(BaseHandler):
	@asynchronous
	def get(self):
		uid = self.get_userid
		myredis = Redis()		#添加接收用户有多少条提醒
		remindNum = myredis.HGet("Remind",uid)
		if not remindNum:
			remindNum = 0
		else:
			remindNum = int(remindNum)
		db = DB()
		sql = "select userid,time,remindTime from audio where\
		 friendid = %d order by time desc limit %d" %(uid,remindNum)
		resultval = db.Query(sql)
		remindlist = []
		for remind in resultval:
			friendid = int(remind[0])
			sql = "select phoneNumber from user where id=%d" % friendid 
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			url = options.host + "/get_remind_audio/?" + "time=" + str(remind[1])
			reminds = {'phoneNumber':phoneNumber,'remindTime':remind[2],'audio':url}
			remindlist.append(reminds)
		#myredis.HSet("Remind",uid,0)
		data = {'status_code':STATUS_OK,'data':remindlist}
		self.api_response(data)
	@asynchronous
	@authenticated
	def post(self):
		uid = self.get_userid
		myredis = Redis()		#添加接收用户有多少条提醒
		remindNum = myredis.HGet("Remind",uid)
		if not remindNum:
			remindNum = 0
		else:
			remindNum = int(remindNum)
		db = DB()
		sql = "select userid,time,remindTime from audio where\
		 friendid = %d order by time desc limit %d" %(uid,remindNum)
		resultval = db.Query(sql)
		remindlist = []
		for remind in resultval:
			friendid = int(remind[0])
			sql = "select phoneNumber from user where id=%d" % friendid 
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			url = options.host + "/get_remind_audio/?" + "time=" + str(remind[1])
			reminds = {'phoneNumber':phoneNumber,'remindTime':remind[2],'audio':url}
			remindlist.append(reminds)
		myredis.HSet("Remind",uid,0)
		data = {'status_code':STATUS_OK,'data':remindlist}
		self.api_response(data)


#返回最近的收到的留言
#获取后，清空
class GetRemindLimitFifty(BaseHandler):
	@asynchronous
	def get(self):
		uid = self.get_userid
		myredis = Redis()		#添加接收用户有多少条提醒
		remindNum = myredis.HGet("Remind",uid)
		if not remindNum:
			remindNum = 0
		else:
			remindNum = int(remindNum)
		db = DB()
		sql = "select userid,time,remindTime from audio where\
		 friendid = %d order by time desc limit 50" %(uid)
		resultval = db.Query(sql)
		remindlist = []
		for remind in resultval:
			friendid = int(remind[0])
			sql = "select phoneNumber from user where id=%d" % friendid 
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			url = options.host + "/get_remind_audio/?" + "time=" + str(remind[1])
			reminds = {'phoneNumber':phoneNumber,'remindTime':remind[2],'audio':url}
			remindlist.append(reminds)
		#myredis.HSet("Remind",uid,0)
		data = {'status_code':STATUS_OK,'data':remindlist}
		self.api_response(data)
	@asynchronous
	@authenticated
	def post(self):
		uid = self.get_userid
		myredis = Redis()		#添加接收用户有多少条提醒
		remindNum = myredis.HGet("Remind",uid)
		if not remindNum:
			remindNum = 0
		else:
			remindNum = int(remindNum)
		db = DB()
		sql = "select userid,time,remindTime from audio where\
		 friendid = %d order by time desc limit 50" %(uid)
		resultval = db.Query(sql)
		remindlist = []
		for remind in resultval:
			friendid = int(remind[0])
			sql = "select phoneNumber from user where id=%d" % friendid 
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			url = options.host + "/get_remind_audio/?" + "time=" + str(remind[1])
			reminds = {'phoneNumber':phoneNumber,'remindTime':remind[2],'audio':url}
			remindlist.append(reminds)
		myredis.HSet("Remind",uid,0)
		data = {'status_code':STATUS_OK,'data':remindlist}
		self.api_response(data)


#返回最近的收到的留言
#获取后，清空
class GetRemindLimitXrange(BaseHandler):
	@asynchronous
	def get(self):
		lrange = int(self.get_argument("lrange"))
		rrange = int(self.get_argument("rrange"))
		if(lrange > rrange):
			lrange,rrange = rrange,lrange
		uid = self.get_userid
		myredis = Redis()		#添加接收用户有多少条提醒
		remindNum = myredis.HGet("Remind",uid)
		if not remindNum:
			remindNum = 0
		else:
			remindNum = int(remindNum)
		db = DB()
		sql = "select userid,time,remindTime from audio where\
		 friendid = %d order by time desc limit %d,%d" %(uid,lrange,rrange)
		resultval = db.Query(sql)
		remindlist = []
		for remind in resultval:
			friendid = int(remind[0])
			sql = "select phoneNumber from user where id=%d" % friendid 
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			url = options.host + "/get_remind_audio/?" + "time=" + str(remind[1])
			reminds = {'phoneNumber':phoneNumber,'remindTime':remind[2],'audio':url}
			remindlist.append(reminds)
		#myredis.HSet("Remind",uid,0)
		data = {'status_code':STATUS_OK,'data':remindlist}
		self.api_response(data)
	@asynchronous
	@authenticated
	def post(self):
		lrange = int(self.get_argument("lrange"))
		rrange = int(self.get_argument("rrange"))
		if(lrange > rrange):
			lrange,rrange = rrange,lrange
		uid = self.get_userid
		myredis = Redis()		#添加接收用户有多少条提醒
		remindNum = myredis.HGet("Remind",uid)
		if not remindNum:
			remindNum = 0
		else:
			remindNum = int(remindNum)
		db = DB()
		sql = "select userid,time,remindTime from audio where\
		 friendid = %d order by time desc limit %d,%d" %(uid,lrange,rrange)
		resultval = db.Query(sql)
		remindlist = []
		for remind in resultval:
			friendid = int(remind[0])
			sql = "select phoneNumber from user where id=%d" % friendid 
			friendvalues = db.Query(sql)
			phoneNumber = friendvalues[0][0]
			url = options.host + "/get_remind_audio/?" + "time=" + str(remind[1])
			reminds = {'phoneNumber':phoneNumber,'remindTime':remind[2],'audio':url}
			remindlist.append(reminds)
		myredis.HSet("Remind",uid,0)
		data = {'status_code':STATUS_OK,'data':remindlist}
		self.api_response(data)
