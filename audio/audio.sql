create database if not exists buddy;
use buddy;

create table audio(
	`userid` int not null,
	`friendid` int not null,
	`time` int not null,
	`remindTime` int not null,
	`send` int DEFAULT 0
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

alter table audio add index(friendid);