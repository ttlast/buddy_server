# coding: utf-8

import os
from tornado.options import define,options

SITE_ROOT = os.path.dirname(__file__)

settings = {
	"static_path":os.path.join(SITE_ROOT,'static'),
	"template_path":os.path.join(SITE_ROOT,'templates'),
	"cookie_secret":"0y7uIjECSxmnnm0QmkCyh5Zpna0/sElYp6H7BxVLn1U=",
	"login_url":"/login/",
	"pycket":{
		'engine':'redis',
		'storage':{
			'host': 'localhost',
			'port': 6379,
			'db_sessions':0,
			'db_notifications': 11,
			#'max_connections': 2147483648,
		},
		'cookies': {
			'expires_days': 120,
		},
	},
	#"xsrf_cookies": True,   调试阶段，先不用
}


define("expire",default=90*24*60*60,help="session expire",type=int) # session expire 3 months
define("mysql_host",default="localhost",help="mysql_host")
define("mysql_user",default="root",help="db_user")
define("mysql_passwd",default="root",help="db_passwd")
define("mysql_db",default="buddy",help="db name")
define("avatarpath",default="/avatar",help="avatarpath")
define("audiopath",default="/buddyaudio",help="user audio save path")
define("host",default="http://127.0.0.1:8888",help="buddy host name")
define("filelimit",default=4*1024*1024,help="file upload limit",type=int)
define("redishost",default="127.0.0.1",help="redis host")
define("redisport",default=6379,help="redis port",type=int)
define("chunksize",default=4*1024,help="cache size",type=int)

STATUS_OK = 1
STATUS_PARAMS = 2 #参数缺失
STATUS_PARAMS_ERROR = 3 #参数类型错误
STATUS_PASS_ERROR = 4  #登陆密码错误
STATUS_NAME_ERROR = 5  #登陆用户名错误
STATUS_FILE_ERROR = 6 #文件过大 or 没有  ps: 任何文件都不能 > 4M 哦
STATUS_FILE_MISS = 7 #文件不见了
STATUS_FRIEND_MISS = 8 #好友没注册哦
STATUS_FRIEND_ACCEPT = 9  #添加好友成功
STATUS_FRIEND_WA = 10 #添加好友失败
STATUS_NOT_LOGIN = 11 #没有登陆


#
TYPE_TELEPHONE = 1  #打电话  6
TYPE_VOICE_REMIND = 2 #语言提醒 3
TYPE_SMS = 3 #短信 3
TYPE_MISS = 4 #发送思念 3
SCORE = {0:0,TYPE_TELEPHONE:6,TYPE_VOICE_REMIND:3,TYPE_SMS:3,TYPE_MISS:3}